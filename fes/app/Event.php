<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Event extends Authenticatable
{
    
    protected $table = "events";//entidad que referencia a events

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ //campos a tratar en el sistema. Campos que Se permite que se rellenen desde el sistema, no desde la base de datos
        'id','building', 'description', 'direction', 'price', 'title', 'resume',  'type',  'event_code',  
        'num_places_total',  'num_places_bussy',  'start_date_birth',  'end_date_birth',  'start_date_inscription',
        'end_date_inscription',  'start_date_period',  'end_date_period',  'descounts',  'suplements',  'way_ofpayment',  'timetable',  'forma_pago',  'category_id', 
    ];
    
    public function users(){
        return $this->hasMany('App\User');
    }
    
    public function payments(){
        return $this->hasMany('App\Payment');
    }
    
    public function category(){
        return $this->belongsTo('App\EventCategory');
    }
}
