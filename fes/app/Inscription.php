<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Inscription extends Authenticatable
{
   
    protected $table = "inscriptions";// tabla 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ //campos a tratar en el sistema. Campos que Se permite que se rellenen desde el sistema, no desde la base de datos
        'id', 'estat', 'curso_id', 'usuari_id', 'sesion_id'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }
    
    public function session() {
        return $this->belongsTo('App\Session');
    }
}
