<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Payment extends Authenticatable
{
    
    protected $table = "payments";//entidad que referencia a users

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ //campos a tratar en el sistema. Campos que Se permite que se rellenen desde el sistema, no desde la base de datos
        'id','number_confirmation', 'payment_status', 'users_id', 'event_id',
    ];
    
    public function user(){
        $this->belongsTo('App\User');//un pago pertenece a un usuario. Un pago es realizado por un usuario
    }
    
    public function event(){
        $this->belongsTo('App\Event');//un pago pertenece a un evento. Un pago es realizado por un evento
    }
}
