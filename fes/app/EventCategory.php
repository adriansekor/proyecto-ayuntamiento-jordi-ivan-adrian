<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class EventCategory extends Authenticatable
{
    
    protected $table = "event_categories";//entidad que referencia a users

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ //campos a tratar en el sistema. Campos que Se permite que se rellenen desde el sistema, no desde la base de datos
        'id','name',
    ];
    
    public function events(){
        return $this->hasMany('App\Event');
    }
}
