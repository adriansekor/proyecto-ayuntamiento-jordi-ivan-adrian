<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Enroll extends Authenticatable
{
    
    protected $table = "events_users";//entidad que referencia a users

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ //campos a tratar en el sistema. Campos que Se permite que se rellenen desde el sistema, no desde la base de datos
        'id','number_inscription', 'installment_payment', 'require_special_atention', 'additional_information', 'authorize_receive_diffusion',
    ];
    
    public function user(){
        return $this->belongsTo('App\User');
    }
    
    public function event(){
        return $this->belongsTo('App\Event');
    }

    
}
