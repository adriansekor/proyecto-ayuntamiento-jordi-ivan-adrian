<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Session extends Authenticatable
{
    
    protected $table = "sessions";// tabla 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ //campos a tratar en el sistema. Campos que Se permite que se rellenen desde el sistema, no desde la base de datos
        'id', 'places', 'estat', 'data_publicacio', 'data_inicial_inscripcio', 'datafinal_inscripcio', 'horari', 'duracio', 'cursos_id'
    ];

    public function course() {
        return $this->belongsTo('App\Course');
    }

    public function inscriptions() {
        return $this->hasMany('App\Inscription');
    }
}
