<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class DepartamentsTenenUsuarisAdmin extends Authenticatable
{
    
    protected $table = "departamentsTenenUsuarisAdmin";// tabla

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ //campos a tratar en el sistema. Campos que Se permite que se rellenen desde el sistema, no desde la base de datos
        'id'
    ];
    
    //UsuarisAdmin tienen MUCHOS DepartamentsTenenUsuarisAdmin
    public function departament(){
        return $this->belongsTo('App\Departaments');
    }

    public function usuariAdmin(){
        return $this->belongsTo('App\UsuarisAdmin');
    }



}
