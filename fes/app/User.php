<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable; //Notifiable -> para decir que ésta entidad esta relacionada con el logueo del usuario, para decir si el usuario se esta logueando
    
    protected $table = "users";// tabla 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ //campos a tratar en el sistema. Campos que Se permite que se rellenen desde el sistema, no desde la base de datos
        'id', 'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ //se encriptan
        'password' //remember_hidden: campo que tiene que ser transformado o codificado. No es un campo aportado por el usuario, lo da el sistema
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    //UsuarisAdmin tienen MUCHOS DepartamentsTenenUsuarisAdmin
    public function inscriptions(){
        return $this->hasMany('App\Inscription');
    }
}
