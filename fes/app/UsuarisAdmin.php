<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UsuarisAdmin extends Authenticatable
{
    use Notifiable; //Notifiable -> para decir que ésta entidad esta relacionada con el logueo del usuario, para decir si el usuario se esta logueando
    
    protected $table = "usuarisAdmin";// tabla usuarisAdmin

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ //campos a tratar en el sistema. Campos que Se permite que se rellenen desde el sistema, no desde la base de datos
        'id', 'dni', 'nom', 'cognom1', 'cognom2', 'data_neixament', 'genere', 'email', 'tipus', 'estat', 'adreca',  'municipi',  'registrat_a_tarragona',  'codi_postal', 'telefon', 'password', 'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ //se encriptan
        'password', 'remember_token', //remember_hidden: campo que tiene que ser transformado o codificado. No es un campo aportado por el usuario, lo da el sistema
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    //UsuarisAdmin tienen MUCHOS DepartamentsTenenUsuarisAdmin
    public function departamentsTenenUsuarisAdmin(){
        return $this->hasMany('App\DepartamentsTenenUsuarisAdmin');
    }
}
