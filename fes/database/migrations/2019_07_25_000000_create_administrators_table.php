<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdministratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrators', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dni');
            $table->string('name');
            $table->string('lastname1');
            $table->string('lastname2');
            $table->date('date_birth');
            $table->string('gender');
            $table->string('email')->unique();
            $table->integer('type'); //Tipus: admin o gestor
            $table->string('status'); //estat: actiu, no actiu o similar
            $table->string('address');
            $table->string('municipality');
            $table->boolean('registered_in_tarragona');
            $table->string('postal_code');
            $table->string('phone');
            $table->string('password');
            $table->rememberToken();//guarda el token que genera el proyecto para cuando se olvida la contraseña
            $table->timestamps();//crea dos columnas: create_at y update_at, tipo fecha
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrators');
    }
}
