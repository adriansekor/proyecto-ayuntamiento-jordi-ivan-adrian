<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();//xra q las rutas puedan autenticar los usuarios

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('cursos', function () {
    $nombre = 'Ivan';
    return view('cursos')->with('nombressss',$nombre);
});


/*Route::post('/contacto/enviar', [
    'uses' => 'ContactController@sendInfo',
    'as' => 'contact.sendInfo'
]);*/


/*login*/
/*Route::get('/password/email', [
    'uses' => 'Auth\ForgotPasswordController@getEmail',
    'as' => 'password.getEmail'
]);
Route::post('/password/email', [
    'uses' => 'Auth\ForgotPasswordController@postEmail',
    'as' => 'password.postEmail'
]);
Route::get('/password/reset/{token}', [
    'uses' => 'Auth\ForgotPasswordController@getReset',
    'as' => 'password.getReset'
]);
Route::post('/password/reset', [
    'uses' => 'Auth\ForgotPasswordController@postReset',
    'as' => 'password.postReset'
]);*/



/*admin panel*/ //MIDDLEWARE
/*Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'admin' ], function () {

    //admin
    Route::get('/', [
        'uses' => 'HomeController@index',
        'as' => '/'
    ]);

    users
    Route::get('/users/', [
        'uses' => 'UserController@index',
        'as' => 'users.main'
    ]);
    Route::get('/users/create', [
        'uses' => 'UserController@create',
        'as' => 'users.create'
    ]);
    Route::post('/users/store', [
        'uses' => 'UserController@store',
        'as' => 'users.store'
    ]);
    Route::get('/users/show/{id}', [
        'uses' => 'UserController@show',
        'as' => 'users.show'
    ]);
    Route::get('/users/edit/{id}', [
        'uses' => 'UserController@edit',
        'as' => 'users.edit'
    ]);
    Route::post('/users/update/{id}', [
        'uses' => 'UserController@update',
        'as' => 'users.update'
    ]);
    Route::get('/users/editpassword/{id}', [
        'uses' => 'UserController@editpassword',
        'as' => 'users.editpassword'
    ]);
    Route::post('/users/updatepassword/{id}', [
        'uses' => 'UserController@updatepassword',
        'as' => 'users.updatepassword'
    ]);
    Route::get('/users/destroy/{id}', [
        'uses' => 'UserController@destroy',
        'as' => 'users.destroy'
    ]);



});*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
