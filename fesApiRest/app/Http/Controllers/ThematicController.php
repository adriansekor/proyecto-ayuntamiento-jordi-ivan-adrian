<?php

namespace App\Http\Controllers;

use App\Thematic;
use Illuminate\Http\Request;

class ThematicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Thematic  $thematic
     * @return \Illuminate\Http\Response
     */
    public function show(Thematic $thematic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Thematic  $thematic
     * @return \Illuminate\Http\Response
     */
    public function edit(Thematic $thematic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Thematic  $thematic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Thematic $thematic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Thematic  $thematic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Thematic $thematic)
    {
        //
    }
}
