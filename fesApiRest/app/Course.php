<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = "courses";// tabla 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ //campos a tratar en el sistema. Campos que Se permite que se rellenen desde el sistema, no desde la base de datos
        'id', 'code', 'name', 'type', 'description', 'addressed_to'
    ];

    public function sessions() {
        return $this->hasMany('App\Session');
    }

    public function department() {
        return $this->belongsTo('App\Department');
    }
}
