<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Delete the table before readding data to it
        DB::table('groups')->truncate();

        DB::table('groups')->insert([
            'name' => 'PHP con laravel',
        ]);

        DB::table('groups')->insert([
            'name' => 'nfi580',
        ]);
    }
}
