<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Delete the table before readding data to it
        DB::table('departments')->truncate();

        DB::table('departments')->insert([
            'name' => 'PHP con laravel',
            'nif' => 'programacion',
            'description' => 'curso de programacion completo',
            'info_responsible' => 'estudiantes',
            'info_passarella' => 'dfadskf',
        ]);

        DB::table('departments')->insert([
            'name' => 'nfi580',
            'nif' => 'curso Java',
            'description' => 'programacion',
            'info_responsible' => 'estudiantes',
            'info_passarella' => 'adsfasdf',
        ]);
    }
}
