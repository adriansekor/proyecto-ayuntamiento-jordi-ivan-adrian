<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DiscountGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Delete the table before readding data to it
        DB::table('discount__groups')->truncate();

        DB::table('discount__groups')->insert([
            'course_id' => 1,
            'department_id' => 2,
            'group_id' => 2,
            'discount_id' => 1,
        ]);

        DB::table('discount__groups')->insert([
            'course_id' => 2,
            'department_id' => 1,
            'group_id' => 2,
            'discount_id' => 2,
        ]);
    }
}
