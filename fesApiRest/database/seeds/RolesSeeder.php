<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Delete the table before readding data to it
        DB::table('roles')->truncate();

        DB::table('roles')->insert([
            'name' => 'PHP con laravel',
        ]);

        DB::table('roles')->insert([
            'name' => 'nfi580',
        ]);
    }
}
