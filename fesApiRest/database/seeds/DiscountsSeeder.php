<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DiscountsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Delete the table before readding data to it
        DB::table('discounts')->truncate();

        DB::table('discounts')->insert([
            'value' => 20,
            'type' => 1,
            'department_id' => 1,
            'course_id' => 2,
        ]);

        DB::table('discounts')->insert([
            'value' => 50,
            'type' => 3,
            'department_id' => 3,
            'course_id' => 1,
        ]);
    }
}
