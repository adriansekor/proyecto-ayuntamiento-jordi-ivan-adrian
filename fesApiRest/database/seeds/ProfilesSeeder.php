<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Delete the table before readding data to it
        DB::table('profiles')->truncate();

        DB::table('profiles')->insert([
            'name' => 'PHP con laravel',
        ]);

        DB::table('profiles')->insert([
            'name' => 'nfi580',
        ]);
    }
}
