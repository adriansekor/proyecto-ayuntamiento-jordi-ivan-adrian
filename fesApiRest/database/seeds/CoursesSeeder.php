<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Delete the table before readding data to it
        DB::table('courses')->truncate();

        DB::table('courses')->insert([
            'code' => 'kdb859',
            'name' => 'PHP con laravel',
            'type' => 'programacion',
            'description' => 'curso de programacion completo',
            'addressed_to' => 'estudiantes',
            'minimum_age' => 14,
            'department_id' => 1,
        ]);

        DB::table('courses')->insert([
            'code' => 'nfi580',
            'name' => 'curso Java',
            'type' => 'programacion',
            'description' => 'programacion completo con tutor para resolver dudas',
            'addressed_to' => 'estudiantes',
            'minimum_age' => 18,
            'department_id' => 2,
        ]);
    }
}
