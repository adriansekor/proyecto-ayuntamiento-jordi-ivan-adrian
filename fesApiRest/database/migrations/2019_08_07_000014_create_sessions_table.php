<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('places'); //plazas del curso
            $table->integer('status'); //estat: activa o no, etc
            $table->date('date_publication');
            $table->date('initial_date_inscription');
            $table->date('due_date_inscription');
            $table->string('timetable'); //horari: dies, hores
            $table->integer('duration');
            $table->timestamps();//crea dos columnas: create_at y update_at, tipo fecha
            $table->integer('course_id')->unsigned();
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}
