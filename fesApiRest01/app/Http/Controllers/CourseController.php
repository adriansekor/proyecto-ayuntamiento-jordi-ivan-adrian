<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use DB;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Course::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $course = new Course();

        $course->code = $data->code;
        $course->name = $data->name;
        $course->type = $data->type;
        $course->description = $data->description;
        $course->addressed_to = $data->addressed_to;
        $course->minimum_age = $data->minimum_age;
        $course->image_url = $data->image_url;
        $course->department_id = $data->department_id;

        $course->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        $data = $request->all();

        $course = Course::where('id','=',$request->id)->first();
        
        $course->code = $data->code;
        $course->name = $data->name;
        $course->type = $data->type;
        $course->description = $data->description;
        $course->addressed_to = $data->addressed_to;
        $course->minimum_age = $data->minimum_age;
        $course->image_url = $data->image_url;
        $course->department_id = $data->department_id;

        $course->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Course $course)
    {
        $course = Course::where('id','=',$request->id)->first();
        $course->delete();
    }

    public function getCourseByProfile(Request $request, Course $course) {
        $sql = DB::table('courses as c')
                ->join('course__profiles as cp','cp.course_id','=','c.id')
                ->join('profiles as p','p.id','=','cp.profile_id')
                ->where('p.id','=','1')
                ->get();
                
    }

    public function getCourseByThematic(Request $request, Course $course) {
        $sql = DB::table('courses as c')
                ->join('course__thematics as ct','ct.course_id','=','c.id')
                ->join('thematics as t','t.id','=','ct.thematics_id')
                ->where('t.id','=','1')
                ->get();
                
    }
}
