<?php

namespace App\Http\Controllers;

use App\Course_Thematic;
use Illuminate\Http\Request;

class CourseThematicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course_Thematic  $course_Thematic
     * @return \Illuminate\Http\Response
     */
    public function show(Course_Thematic $course_Thematic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course_Thematic  $course_Thematic
     * @return \Illuminate\Http\Response
     */
    public function edit(Course_Thematic $course_Thematic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course_Thematic  $course_Thematic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course_Thematic $course_Thematic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course_Thematic  $course_Thematic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course_Thematic $course_Thematic)
    {
        //
    }
}
