<?php

namespace App\Http\Controllers;

use App\Department_User;
use Illuminate\Http\Request;

class DepartmentUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department_User  $department_User
     * @return \Illuminate\Http\Response
     */
    public function show(Department_User $department_User)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department_User  $department_User
     * @return \Illuminate\Http\Response
     */
    public function edit(Department_User $department_User)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department_User  $department_User
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department_User $department_User)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department_User  $department_User
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department_User $department_User)
    {
        //
    }
}
