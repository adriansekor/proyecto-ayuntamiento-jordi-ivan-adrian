<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscription extends Model
{
    protected $table = "sessions";// tabla 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ //campos a tratar en el sistema. Campos que Se permite que se rellenen desde el sistema, no desde la base de datos
        'id', 'status', 'curso_id', 'user_id', 'session_id'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function session() {
        return $this->belongsTo('App\Session');
    }
}
