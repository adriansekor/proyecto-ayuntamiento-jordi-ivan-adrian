<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        //desactivar revision de las foreign keys
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        //eliminar usuarios de la tabla
        //DB::table($table)->truncate();


        $this->call(DepartmentsSeeder::class);
        $this->call(ProfilesSeeder::class);
        $this->call(ThematicsSeeder::class);
        $this->call(GroupsSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(DepartmentUsersSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(DepartmentsSeeder::class);
        $this->call(CoursesSeeder::class);
        $this->call(CourseProfilesSeeder::class);
        $this->call(CourseThematicsSeeder::class);
        $this->call(DiscountsSeeder::class);
        $this->call(DiscountGroupsSeeder::class);
        $this->call(SessionsSeeder::class);
        $this->call(InscriptionsSeeder::class);
    }
}
