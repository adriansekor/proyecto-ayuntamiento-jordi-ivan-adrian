<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Delete the table before readding data to it
        DB::table('users')->truncate();

        DB::table('users')->insert([
            'name' => 'PHP con laravel',
            'email' => 'dddd@gmail.com',
            'password' => bcrypt('123456789'),
        ]);

        DB::table('users')->insert([
            'name' => 'nfi580',
            'email' => 'asdfasd@gmail.com',
            'password' => bcrypt('123456789'),
        ]);
    }
}
