<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Delete the table before readding data to it
        DB::table('department__users')->truncate();

        DB::table('department__users')->insert([
            'department_id' => 2,
            'user_id' => 1,
        ]);

        DB::table('department__users')->insert([
            'department_id' => 2,
            'user_id' => 2,
        ]);
    }
}
