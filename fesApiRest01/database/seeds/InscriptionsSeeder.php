<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InscriptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ///Delete the table before readding data to it
        DB::table('inscriptions')->truncate();

        DB::table('inscriptions')->insert([
            'status' => 2,
            'curso_id' => 1,
            'user_id' => 2,
            'session_id' => 1,
        ]);

        DB::table('inscriptions')->insert([
            'status' => 2,
            'curso_id' => 1,
            'user_id' => 2,
            'session_id' => 1,
        ]);
    }
}
