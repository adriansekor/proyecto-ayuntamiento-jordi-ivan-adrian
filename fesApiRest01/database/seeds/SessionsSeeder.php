<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SessionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Delete the table before readding data to it
        DB::table('sessions')->truncate();

        DB::table('sessions')->insert([
            'places' => 100,
            'status' => 2,
            'date_publication' => '2020-01-10',
            'initial_date_inscription' => '2020-01-10',
            'due_date_inscription' => '2019-01-20',
            'timetable' => 'De dilluns a divendres, de 10 a 14h',
            'duration' => 150,
            'course_id' => 1,
        ]);

        DB::table('sessions')->insert([
            'places' => 100,
            'status' => 1,
            'date_publication' => '2020-01-10',
            'initial_date_inscription' => '2020-01-10',
            'due_date_inscription' => '2019-01-20',
            'timetable' => 'De dilluns a divendres, de 10 a 14h',
            'duration' => 120,
            'course_id' => 2,
        ]);
    }
}
