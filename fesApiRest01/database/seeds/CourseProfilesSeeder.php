<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CourseProfilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Delete the table before readding data to it
        DB::table('course__profiles')->truncate();

        DB::table('course__profiles')->insert([
            'profile_id' => 1,
            'course_id' => 1,
        ]);

        DB::table('course__profiles')->insert([
            'profile_id' => 1,
            'course_id' => 2,
        ]);
    }
}
