<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ThematicsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Delete the table before readding data to it
        DB::table('thematics')->truncate();

        DB::table('thematics')->insert([
            'name' => 'PHP con laravel',
        ]);

        DB::table('thematics')->insert([
            'name' => 'nfi580',
        ]);
    }
}
