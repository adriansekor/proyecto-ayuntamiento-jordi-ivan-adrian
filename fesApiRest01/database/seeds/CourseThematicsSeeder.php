<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CourseThematicsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Delete the table before readding data to it
        DB::table('course__thematics')->truncate();

        DB::table('course__thematics')->insert([
            'department_id' => 2,
            'course_id' => 1,
            'thematic_id' => 2,
        ]);

        DB::table('course__thematics')->insert([
            'department_id' => 1,
            'course_id' => 2,
            'thematic_id' => 1,
        ]);
    }
}
