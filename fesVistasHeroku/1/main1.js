
//Crear objeto ajax para conectar con php 
function objetoAjax() {
	var xmlhttp = false;

	try{
        //Create ajax object
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try{
            //Create ajax object
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(E){
			xmlhttp = false;
		}
	}

	if(!xmlhttp && typeof XMLHttpRequest != 'undefined')
	{
        //Create ajax object
		xmlhttp = new XMLHttpRequest();
	}

	return xmlhttp;
}

function enviar_datos_usuario() {
    var name = document.nuevo_usuario.username.value;
    var email = document.nuevo_usuario.email.value;
    var password = document.nuevo_usuario.password.value;
    var c_password = document.nuevo_usuario.c_password.value;

    //crear obj ajax
    var ajax = objetoAjax();
    ajax.open('POST', 'register.php', true);
    ajax.onreadystatechange = function(){
        if(ajax.readyState == 4) {
            alert('usuario agregado');
        }
    }

    ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    //parametros
    ajax.send(
        'name=' + name +
        '&email=' + email +
        '&password=' + password +
        '&c_password=' + c_password 

    );
}




