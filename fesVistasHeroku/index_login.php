<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <!-- Estilos -->
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/icons.css">
    <script type="text/javascript" src="mainLogin.js"></script>
    <title>Inici de sessió</title>
</head>

<body>
    <?php
    include 'header.html';
    ?>
    <div class="container">
        <div class="row loginDiv">
            <div class="col-3"></div>
            <div class="col-6">
                <form class="form-signin" action="loginLogin.php" method="post"  id="formulario_login">
                    <h1 class="h4 mb-3 font-weight-normal">Iniciar sessió</h1>
                    <div class="trans text-center">
                        <input type="email" name="username" id="username" class="form-control" placeholder="Correu electrònic" required>
                    </div>
                    <input type="password" name="password" id="password" class="form-control" placeholder="Contrassenya" required>

                    <button class="btn btn-success btn-block" type="submit" id="loguear">Entrar</button>
                    <br>
                    <a href="#">Has oblidat la teva contrassenya?</a>
                </form>
            </div>
            <div class="col-3"></div>
        </div>
        <hr>
        <div class="row loginDiv">
            <div class="col-3"></div>
            <div class="col-6">
                <p>Encara no tens un compte? Registra't <a href="#">aquí</a>
            </div>
            <div class="col-3"></div>
        </div>
    </div>
    <?php
    include 'footer.html';
    ?>
</body>

</html>