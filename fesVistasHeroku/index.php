<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <!-- Estilos -->
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/icons.css">
    <script type="text/javascript" src="main.js"></script> 
    <title>Inici de sessió</title>
</head>

<body>

<?php
    include 'header.html';
    echo 'asldkjfhajsdf';
?>
<div class="container">
        <div class="row loginDiv">
            <div class="col-3"></div>
            <div class="col-6">
                <form class="form-signin" name="nuevo_usuario" id="formulario_registro" action="" onsubmit="enviar_datos_usuario(); return false;">
                    <h1 class="h4 mb-3 font-weight-normal">Iniciar sessió</h1>
                    <div class="trans text-center">
                        <input type="text" name="username" id="username" class="form-control" placeholder="agregar nombre" required autofocus>
                    </div>
                    <div class="trans text-center">
                        <input type="email" name="email" id="email" class="form-control" placeholder="Correu electrònic" required>
                    </div>
                    <input type="password" name="password" id="password" class="form-control" placeholder="Contrassenya" required>

                    <input type="password" name="c_password" id="c_password" class="form-control" placeholder="confirmar_contrassenya" required>

                    <button class="btn btn-success btn-block" type="submit">Entrar</button>
                    <br>
                    <a href="#">Has oblidat la teva contrassenya?</a>
                </form>
            </div>
            <div class="col-3"></div>
        </div>
        <hr>
        <div class="row loginDiv">
            <div class="col-3"></div>
            <div class="col-6">
                <p>Encara no tens un compte? Registra't <a href="#">aquí</a>
            </div>
            <div class="col-3"></div>
        </div>
    </div>

    <?php
    include 'footer.html';
    ?>

</body>

</html>