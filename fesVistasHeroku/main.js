
//Crear objeto ajax para conectar con php 
function objetoAjax() {
	var xmlhttp = false;

	try{
        //Create ajax object
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try{
            //Create ajax object
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(E){
			xmlhttp = false;
		}
	}

	if(!xmlhttp && typeof XMLHttpRequest != 'undefined')
	{
        //Create ajax object
		xmlhttp = new XMLHttpRequest();
	}

	return xmlhttp;
}

addEventListener('load', inicializarEventos,false);

function inicializarEventos() {
    var ob = document.getElementById("formulario_registro");
    ob.addEventListener('submit',enviarDatos,false);
}

function enviarDatos(e) {
    e.preventDefault();
    enviarFormulario();
}

function encadenarDatos() {
    var cadena = '';
    var name = document.getElementById('username').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    var c_password = document.getElementById('c_password').value;
    cadena = 'name=' + name + '&email=' + email + '&password=' + password + '&c_password=' + c_password
    return cadena;
}

function enviarFormulario() {
    var ajax = objetoAjax();
    ajax.open('POST', 'register.php', true);
    ajax.onreadystatechange = function(){
        if(ajax.readyState == 4) {
            alert('usuario agregado correctamente');
        }
    }
    ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    ajax.send(encadenarDatos());
}



