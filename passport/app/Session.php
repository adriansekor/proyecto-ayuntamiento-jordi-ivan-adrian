<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $table = "sessions";// tabla 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ //campos a tratar en el sistema. Campos que Se permite que se rellenen desde el sistema, no desde la base de datos
        'id', 'places', 'status', 'date_publication', 'initial_date_inscription', 'due_date_inscription', 'timetable', 'duration', 'course_id'
    ];

    public function course() {
        return $this->belongsTo('App\Course');
    }

    public function inscriptions() {
        return $this->hasMany('App\Inscription');
    }
}
