<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = "courses";// tabla 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ //campos a tratar en el sistema. Campos que Se permite que se rellenen desde el sistema, no desde la base de datos
        'id', 'name', 'email', 'type', 'who_is_it_for', 'minimum_age', 'code', 'department_id'
    ];

    public function sessions() {
        return $this->hasMany('App\Session');
    }

    public function department() {
        return $this->belongsTo('App\Department');
    }
}
