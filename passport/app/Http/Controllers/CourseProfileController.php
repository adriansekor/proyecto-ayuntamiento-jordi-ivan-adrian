<?php

namespace App\Http\Controllers;

use App\Course_Profile;
use Illuminate\Http\Request;

class CourseProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course_Profile  $course_Profile
     * @return \Illuminate\Http\Response
     */
    public function show(Course_Profile $course_Profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course_Profile  $course_Profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Course_Profile $course_Profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course_Profile  $course_Profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course_Profile $course_Profile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course_Profile  $course_Profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course_Profile $course_Profile)
    {
        //
    }
}
