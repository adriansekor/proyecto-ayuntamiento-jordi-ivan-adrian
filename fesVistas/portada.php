	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Inicio</title>
		<script src="https://kit.fontawesome.com/3cf921f7ed.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"> </script>
		<script type="text/javascript" src="js/bootstrap.min.js"> </script>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">

		<!-- Material Design Bootstrap -->
		<link href="css/mdb.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/estilo.css">

	</head>

	<body>


		<!-- SCRIPTS -->

		<!-- JQuery -->
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

		<!-- Tooltips -->
		<!-- <script type="text/javascript" src="https://mdbootstrap.com/previews/docs/latest/js/popper.min.js"></script> -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

		<!-- Bootstrap core JavaScript -->
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
		<!-- MDB core JavaScript -->
		<!-- <script type="text/javascript" src="https://mdbootstrap.com/previews/docs/latest/js/mdb.min.js"></script> -->
		<script type="text/javascript" src="js/mdb.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<!--FIN SCRIPTS -->

		<!-- INICIO HEADER -->
		<?php
		include 'header.html';
		?>
		<!-- FIN HEADER -->

		<!-- INICIO PORTADA -->
		<div id="portada">
			<br>
			<h2 class="text-danger portada-title">Cursos d'inminent inscripció</h2>

			<!-- INICIO CARRUSEL -->







			<!--Carousel de Cursos-->
			<div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">

				<!--Controls-->
				<div class="controls-top">

				</div>
				<!--/.Controls-->

				<!--Indicators-->
				<ol class="carousel-indicators">
					<li data-target="#multi-item-example" data-slide-to="0" class="active"></li>
					<li data-target="#multi-item-example" data-slide-to="1"></li>
					<li data-target="#multi-item-example" data-slide-to="2"></li>
				</ol>
				<!--/.Indicators-->

				<!--Slides-->
				<div class="carousel-inner" role="listbox">

					<!--Primer slide-->
					<div class="carousel-item active">


						<!--Primer slide Curso1-->
						<div class="col-md-4 view overlay">
							<div class="example hoverable">
								<div class="card mb-2">
									<img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg" alt="Card image cap">
									<div class="card-body">
										<h4 class="card-title">Card title</h4>
										<p class="card-text">Some quick example text to build on the card title and make up the bulk of the
											card's content.</p>
										<a class="btn btn-danger">Veure</a>
									</div>
								</div>
							</div>
						</div>
						<!--Fin Primer slide Curso1-->


						<!--Primer slide Curso2-->
						<div class="col-md-4">
							<div class="example hoverable">
								<div class="card mb-2">
									<img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(18).jpg" alt="Card image cap">
									<div class="card-body">
										<h4 class="card-title">Card title</h4>
										<p class="card-text">Some quick example text to build on the card title and make up the bulk of the
											card's content.</p>
										<a class="btn btn-danger">Veure</a>
									</div>
								</div>
							</div>
						</div>
						<!--Fin Primer slide Curso2-->

						<!--Primer slide Curso3-->
						<div class="col-md-4">
							<div class="example hoverable">
								<div class="card mb-2">
									<img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).jpg" alt="Card image cap">
									<div class="card-body">
										<h4 class="card-title">Card title</h4>
										<p class="card-text">Some quick example text to build on the card title and make up the bulk of the
											card's content.</p>
										<a class="btn btn-danger">Veure</a>
									</div>
								</div>
							</div>
						</div>
						<!--Fin Primer slide Curso2-->


					</div>
					<!--/.Fin Primer slide-->

					<!--Segundo slide-->
					<div class="carousel-item ">
						<!--Segundo slide curso1-->
						<div class="col-md-4">
							<div class="example hoverable">
								<div class="card mb-2 ">
									<img class="card-img-top " src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(60).jpg" alt="Card image cap">
									<div class="card-body">
										<h4 class="card-title">Card title</h4>
										<p class="card-text">Some quick example text to build on the card title and make up the bulk of the
											card's content.</p>
										<a class="btn btn-danger">Veure</a>
									</div>
								</div>
							</div>
						</div>
						<!--Fin Segundo slide curso1-->

						<!--Segundo slide curso2-->
						<div class="col-md-4">
							<div class="example hoverable">
								<div class="card mb-2">
									<img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(47).jpg" alt="Card image cap">
									<div class="card-body">
										<h4 class="card-title">Card title</h4>
										<p class="card-text">Some quick example text to build on the card title and make up the bulk of the
											card's content.</p>
										<a class="btn btn-danger">Veure</a>
									</div>
								</div>
							</div>
						</div>

						<!--Fin Segundo slide curso2-->

						<!--Segundo slide curso3-->
						<div class="col-md-4">
							<div class="example hoverable">
								<div class="card mb-2">
									<img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/City/4-col/img%20(48).jpg" alt="Card image cap">
									<div class="card-body">
										<h4 class="card-title">Card title</h4>
										<p class="card-text">Some quick example text to build on the card title and make up the bulk of the
											card's content.</p>
										<a class="btn btn-danger">Veure</a>
									</div>
								</div>
							</div>
						</div>
						<!--Fin Segundo slide curso3-->
					</div>
					<!--/.Fin Segundo slide-->

					<!--Tercer slide-->
					<div class="carousel-item">
						<!--Tercer slide Curso1-->
						<div class="col-md-4">
							<div class="example hoverable">
								<div class="card mb-2">
									<img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(53).jpg" alt="Card image cap">
									<div class="card-body">
										<h4 class="card-title">Card title</h4>
										<p class="card-text">Some quick example text to build on the card title and make up the bulk of the
											card's content.</p>
										<a class="btn btn-danger">Veure</a>
									</div>
								</div>
							</div>
						</div>
						<!--Fin Tercer slide Curso1-->

						<!--Tercer slide Curso2-->
						<div class="col-md-4">
							<div class="example hoverable">
								<div class="card mb-2">
									<img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(45).jpg" alt="Card image cap">
									<div class="card-body">
										<h4 class="card-title">Card title</h4>
										<p class="card-text">Some quick example text to build on the card title and make up the bulk of the
											card's content.</p>
										<a class="btn btn-danger">Veure</a>
									</div>
								</div>
							</div>
						</div>
						<!-- Fin Tercer slide Curso2-->


						<!--Tercer slide Curso3-->
						<div class="col-md-4">
							<div class="example hoverable">
								<div class="card mb-2">
									<img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Food/4-col/img%20(51).jpg" alt="Card image cap">
									<div class="card-body">
										<h4 class="card-title">Card title</h4>
										<p class="card-text">Some quick example text to build on the card title and make up the bulk of the
											card's content.</p>
										<a class="btn btn-danger">Veure</a>
									</div>
								</div>
							</div>
						</div>
						<!-- Fin Tercer slide Curso3-->
					</div>
					<!--/.Tercer slide-->

				</div>
				<!--/.Fin Slides-->

			</div>
			<!--/. Fin Carousel de Cursos-->

			<!--/Button ver todos los cursos-->
			<div class="trans text-center">
				<a class="btn btn-danger">Veure tots els cursos</a>
			</div>
			<!--/.FIIN PORTADA-->
		</div>
		<!-- INICIO FOOTER -->
		<?php
		include 'footer.html';
		?>
		<!-- FIN FOOTER -->
	</body>