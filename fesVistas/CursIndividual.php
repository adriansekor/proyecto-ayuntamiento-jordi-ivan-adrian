	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Inicio</title>
		<script src="https://kit.fontawesome.com/3cf921f7ed.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"> </script>
		<script type="text/javascript" src="js/bootstrap.min.js"> </script>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/estilo.css">

		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">

		<!-- Material Design Bootstrap -->
		<link href="css/mdb.min.css" rel="stylesheet">

		<!-- Stylesheets -->
		<link rel="stylesheet" href="css/bootstrap.min.css" />
		<link rel="stylesheet" href="css/font-awesome.min.css" />
		<link rel="stylesheet" href="css/owl.carousel.css" />
		<link rel="stylesheet" href="css/style.css" />
	</head>

	<body>


		<!-- SCRIPTS -->

		<!-- JQuery -->
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

		<!-- Tooltips -->
		<!-- <script type="text/javascript" src="https://mdbootstrap.com/previews/docs/latest/js/popper.min.js"></script> -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

		<!-- Bootstrap core JavaScript -->
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
		<!-- MDB core JavaScript -->
		<!-- <script type="text/javascript" src="https://mdbootstrap.com/previews/docs/latest/js/mdb.min.js"></script> -->
		<script type="text/javascript" src="js/mdb.min.js"></script>
		<!--FIN SCRIPTS -->


		<!-- INICIO HEADER -->
		<?php
		include 'header.html';
		?>
		<!-- FIN HEADER -->



		<!-- INICIO PORTADA -->

		<!-- INICIO CARRUSEL -->







		<!--Carousel de Cursos-->

		<!-- Page Preloder -->
		<div id="preloder">
			<div class="loader"></div>
		</div>

		<!-- single course section -->
		<section class="single-course spad pb-0">
			<div class="container">
				<div class="course-meta-area">
					<div class="row">
						<div class="col-lg-10 offset-lg-1">

							<h3>Curs de cuina per principiants</h3>
							<div class="course-metas">
								<div class="course-meta">
									<div class="course-author">
										<div class="ca-pic set-bg" data-setbg="img/authors/2.jpg"></div>
										<h6>Profesor/Departament</h6>
										<p>Jose Luis, <span>Cheff</span></p>
									</div>
								</div>
								<div class="course-meta">
									<div class="cm-info">
										<h6>Categoria</h6>
										<p>Cultura</p>
									</div>
								</div>
								<div class="course-meta">
									<div class="cm-info text-success">
										<h6>Places</h6>
										<p>20 places</p>
									</div>
									<p class="text-success font-weight-bold">15 lliures</p>

								</div>
								<div class="course-meta">
									<div class="cm-info">
										<h6>Durada</h6>
										<p>10/Desembre/2019
											<br>11/Desembre/2019
										</p>
									</div>
								</div>
							</div>
							<a href="#" class="site-btn price-btn">Preu: 35€</a>
							<a href="#" class="site-btn buy-btn">Compra aquest curs</a>
							<a href="#" class="site-btn buy-btn float-right">Contacta</a>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-10 offset-lg-1 course-list">
						<div class="cl-item">
							<img src="img/cocina.jpg" height="400px" width="100%" alt="" class="course-preview mx-auto d-block">
							<h4>Descripció del curs</h4>
							<p>Lorem ipsum dolor sit amet, consectetur. Phasellus sollicitudin et nunc eu efficitur. Sed ligula nulla, molestie quis ligula in, eleifend rhoncus ipsum. Donec ultrices, sem vel efficitur molestie, massa nisl posuere ipsum, ut vulputate mauris ligula a metus. Aenean vel congue diam, sed bibendum ipsum. Nunc vulputate aliquet tristique. Integer et pellentesque urna. Lorem ipsum dolor sit amet, consectetur. Phasellus sollicitudin et nunc eu efficitur. Sed ligula nulla, molestie quis ligula in, eleifend rhoncus ipsum. </p>
						</div>

					</div>
				</div>
			</div>
		</section>
		<!-- single course section end -->


		<!-- Page -->

		<!-- Page end -->


		<!-- INICIO FOOTER -->
		<?php
		include 'footer.html';
		?>
		<!-- FIN FOOTER -->


		<!--====== Javascripts & Jquery ======-->
		<script src="js/jquery-3.2.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/mixitup.min.js"></script>
		<script src="js/circle-progress.min.js"></script>
		<script src="js/owl.carousel.min.js"></script>
		<script src="js/main.js"></script>

	</body>