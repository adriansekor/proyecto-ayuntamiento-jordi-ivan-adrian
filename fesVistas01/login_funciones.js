//Crear objeto ajax para conectar con php 
function objetoAjax() {
	var xmlhttp = false;

	try{
        //Create ajax object
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try{
            //Create ajax object
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(E){
			xmlhttp = false;
		}
	}

	if(!xmlhttp && typeof XMLHttpRequest != 'undefined')
	{
        //Create ajax object
		xmlhttp = new XMLHttpRequest();
	}

	return xmlhttp;
}

addEventListener('load',inicializarEventos,false);

function inicializarEventos() {
    var ob = document.getElementById("formulario_login");
    ob.addEventListener('submit',enviarDatos,false);
}

function enviarDatos(e) {
    e.preventDefault();
    enviarFormulario();
}

function encadenarDatos() {
    var cadena = '';
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    cadena = 'username=' + username + '&password=' + password
    return cadena;
}

function enviarFormulario() {
    var ajax = objetoAjax();
    ajax.open('POST', 'login_conexion.php', true);
    ajax.onreadystatechange = function(){
        if(ajax.readyState == 4) {
            alert('usuario logueado correctamente');
            document.getElementsByClassName('debug')[0].innerText=ajax.response;
            
        }
    }
    ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    ajax.send(encadenarDatos());
}



