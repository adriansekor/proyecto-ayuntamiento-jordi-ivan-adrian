<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <!-- Estilos -->
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/icons.css">
    <script type="text/javascript" src="registro_funciones.js"></script>
    <title>Inici de sessió</title>
</head>

<body>
    <?php
    include 'header.html';
    ?>
    <div class="container">
        <div class="row loginDiv">
            <div class="col-3"></div>
            <div class="col-6">
                <!--<form class="form-signin">
                    <h1 class="h4 mb-3 font-weight-normal">Registre s'usuari</h1>
                    <div class="trans text-center">
                        <input type="email" id="loginEmail" class="form-control" placeholder="Nom" required autofocus>
                    </div>
                    <input type="text" id="loginPassword" class="form-control" placeholder="Cognoms" required>

                    <input type="text" id="loginPassword" class="form-control" placeholder="edat" required>

                    <input type="text" id="loginPassword" class="form-control" placeholder="telefon" required>

                    <input type="text" id="loginPassword" class="form-control" placeholder="Correu electrònic" required>

                    <input type="password" id="loginPassword" class="form-control" placeholder="Contrasenya" required>
                    <input type="password" id="loginPassword" class="form-control" placeholder="Confirmar contrasenya" required>

                    
                        <br>

                    <button class="btn btn-success btn-block" type="submit">Enviar</button>
                    <br>
                
                </form>-->
                <form class="form-signin" id="formulario_registro" action="">
                    <h1 class="h4 mb-3 font-weight-normal">Iniciar sessió</h1>
                    <div class="trans text-center">
                        <input type="text" name="username" id="username" class="form-control" placeholder="nom" autofocus>
                    </div>
                    <div class="trans text-center">
                        <input type="email" name="email" id="email" class="form-control" placeholder="Correu electrònic" required>
                    </div>
                    <input type="password" name="password" id="password" class="form-control" placeholder="Contrassenya" required>

                    <input type="password" name="c_password" id="c_password" class="form-control" placeholder="confirmar contrassenya" required>

                    <button class="btn btn-success btn-block" type="submit">Entrar</button>
                    <br>
                    <a href="#">Has oblidat la teva contrassenya?</a>
                </form>
            </div>
            <div class="col-3"></div>
        </div>
        <hr>
        <div class="row loginDiv">
            <div class="col-3"></div>
            <div class="col-6">
                <br>
                <br>
                <p>Ja tens un compte? Inicia sessió <a href="login.php">aquí</a>


            </div>
            <div class="col-3"></div>
        </div>
    </div>
    <?php
    include 'footer.html';
    ?>
</body>

</html>